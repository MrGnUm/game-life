import random
import copy


# Класс "игрового" поля
# Знает свои размеры, умеет работать с объектом Cell. Умеет себя щаполнять этими объектами как по карте, так и по
# переданным размерам поля.
# Само поле представляет собой сетку, связанную по всем краям. Левый с Правым, Верхний с Нижним. Эдакий Тор получается.
class Field:
    map = None
    m = 0
    n = 0

    #  Конструктор передаем Карту(Матрицу), или же размер поля(MxN) и тогда карта будет произвольная (random)
    #  N - строки, M - Столбцы.
    def __init__(self, m=10, n=10, matrix=None):
        # Если не передали матрицу, генерируем произвольную карту
        if matrix is None:
            self.set_m(m)
            self.set_n(n)
            self.map = self.randomize_map()
        # Если передали матрицу, то проверяем на корректность и если все ок, заполняем основыне свойства
        else:
            if self.check_map(matrix):
                self.set_n(len(matrix))
                # После проверки, длинна всех строк одинакова, иначе бы не прошло проверку.
                # Поэтому берем размер первой строки, т.к. она точно есть
                self.set_m(len(matrix[0]))
                self.set_map(self.init_map(matrix))
            else:
                # Сюдя не попадет из-за исключений в методе check_map, но если вдруг в том методе будет False
                # без исключения, в будущем - обработка уже есть
                raise Exception('Incorrect Map!')

    # Проверяем корреткность карты, что количество элементов в строках совпадает, т.к. нам могут передать
    # некорреткную карту. Можно конечно заполнять оставшееся нулями, но мы сообщим об ошибке.
    # Так же проверяем, что матрица не пустая
    def check_map(self, map):
        # Если передали пустую матрицу - выкидываем ошибку
        if len(map) == 0:
            raise Exception('Error! Map length is empty!')
        row_length = len(map[0])
        # Если количество элементов в строке равно 0 - тоже ошибка
        if row_length == 0:
            raise Exception('Error! Map row length is empty!')
        for i in range(1, len(map)):
            # Если же хоть какая-то строка отличается по количеству элементов - ошибка
            if len(map[i]) != row_length:
                raise Exception('Error! Map is incorrect!')
        return True

    # Проинициализировать карту объектами Cell основываясь на матрице, что прислали и уже проверили
    def init_map(self, matrix):
        map = []
        for i in range(0, self.get_n()):
            map.append([0] * self.get_m())
            for j in range(0, self.get_m()):
                # создаем объект Cell. передает координаты и признак (0 | 1) живая ли клетка
                map[i][j] = Cell(i, j, matrix[i][j])
        return map

    # Создаем произвольную карту, размерности MxN и заполняем объектами Cell
    def randomize_map(self):
        map = []
        for i in range(0, self.get_n()):
            map.append([0] * self.get_m())
            for j in range(0, self.get_m()):
                # создаем объект Cell. передает координаты и признак (0 | 1) живая ли клетка
                map[i][j] = Cell(i, j, random.randint(0, 1))
        return map

    # Получить всех соседей клетки
    def get_neigbors(self, cell):
        if not isinstance(cell, Cell):
            raise Exception('Error! not Cell object given')
        return [
            self.get_cell_by_ij(cell.get_x()-1, cell.get_y()-1),
            self.get_cell_by_ij(cell.get_x()-1, cell.get_y()),
            self.get_cell_by_ij(cell.get_x()-1, cell.get_y()+1),
            self.get_cell_by_ij(cell.get_x(), cell.get_y()-1),
            self.get_cell_by_ij(cell.get_x(), cell.get_y()+1),
            self.get_cell_by_ij(cell.get_x()+1, cell.get_y()-1),
            self.get_cell_by_ij(cell.get_x()+1, cell.get_y()),
            self.get_cell_by_ij(cell.get_x()+1, cell.get_y()+1),
        ]

    # Получить количество живых соседей
    def get_alive_neigbor_count(self, cell):
        count = 0
        for item in self.get_neigbors(cell):
            if item.is_alive():
                count += 1
        return count

    # получить количество мертвых (пустых) соседей
    def get_dead_neigbor_count(self, cell):
        count = 0
        for item in self.get_neigbors(cell):
            if not item.is_alive():
                count += 1
        return count

    # Реализуем замкнутость поля, т.е. если уходим по индексам за пределы поля, считаем с другой стороны
    def get_cell_by_ij(self, i, j):
        # Приводим i и j в размерность нашего поля, если выходит за пределы, просто происходит зацикливание
        # Например, размер поля 5x5, и мы передадим -2, то будет значение 3 (учитывая что считаются с нуля - все верно)
        # Или если передадим 5 и размерность 5, значит нам нужне первый элемент, а его номер - 0.
        i = i % self.get_n()
        j = j % self.get_m()
        # Возвращает элемент (объект Cell) по полученным i и j
        return self.get_map()[i][j]

    # Возвращает текущее количество живых клеток
    def get_alive_cell_count(self):
        count = 0
        for i in range(0, self.get_n()):
            for j in range(0, self.get_m()):
                if self.get_map()[i][j].is_alive():
                    count += 1
        return count

    # Выводим карту и её размерность
    def print_map(self, print_m_n=True):
        if print_m_n:
            print('M = {}'.format(self.get_m()))
            print('N = {}'.format(self.get_n()))
        print('alive cells: {}'.format(str(self.get_alive_cell_count())))
        #  Выводим символ "-" в количестве M + 2 (+2 за боквые линии)
        N = self.get_m() + 2
        print('_' * N)
        for row in self.get_map():
            # Жуткая конструкция %), зато работает и компактно =)
            print('|' + ''.join([str('*' if elem.is_alive() else ' ') for elem in row]) + '|')
        print('_' * N, '\n')

    # Сравниваем текущую карту, с тем что передали; true - они равны, false - не равны
    # Допущение - размерности карты одинаковы.
    def is_equal_map(self, cmp_filed):
        my_field = self.get_map()
        for i in range(0, self.get_n()):
            for j in range(0, self.get_m()):
                if my_field[i][j].is_alive() != cmp_filed[i][j].is_alive():
                    return False
        return True

    # Getters and Setters. Возможно лучше было бы использовать @property
    def get_map(self):
        return self.map

    def set_map(self, map):
        self.map = map

    def get_m(self):
        return self.m

    def set_m(self, m):
        if not isinstance(m, int):
            raise Exception('m is not a number!')
        else:
            if m <= 0:
                raise Exception('m is below zero or equal zero!')
        self.m = m

    def get_n(self):
        return self.n

    def set_n(self, n):
        if not isinstance(n, int):
            raise Exception('n is not a number!')
        else:
            if n <= 0:
                raise Exception('n is below zero or equal zero!')
        self.n = n


# Класс ячейки поля
# Сама ячейка знает, свои координаты и свое состояние Живая или Мертвая
class Cell:
    x = None
    y = None
    alive = False

    def __init__(self, x, y, alive=False):
        self.set_x(x)
        self.set_y(y)
        if alive:
            self.born()

    # Getters and Setters. Возможно лучше было бы использовать @property
    def get_x(self):
        return self.x

    def set_x(self, x):
        if isinstance(x, int) and x >= 0:
            self.x = x
        else:
            raise Exception('Incorrect x given!')

    def get_y(self):
        return self.y

    def set_y(self, y):
        if isinstance(y, int) and y >= 0:
            self.y = y
        else:
            raise Exception('Incorrect y given!')

    def is_alive(self):
        return self.alive

    def born(self):
        self.alive = True

    def die(self):
        self.alive = False


# Класс реализующий логику работы с полем и клетками
class RunLife:
    field = None
    previous_maps = []

    # Создаем объект RunLife и сразу инициализуем Поле
    def __init__(self, m=40, n=20, matrix=None):
        if matrix is None:
            self.set_field(Field(m,n))
        else:
            self.set_field(Field(matrix=matrix))

    # основной метод для запуска
    def run(self):
        try:
            self.get_field().print_map()
            # Добавляем первую конфигурацию, в список прошлых конфигураций
            self.add_to_previous_maps(self.get_field().get_map())
            while True:
                new_map = self.next_step()
                self.get_field().set_map(new_map)
                print('Iteration: {:0.0f}'.format(len(self.get_previous_maps())))
                self.get_field().print_map()
                if self.is_need_to_stop():
                    print('Game Over =)')
                    break
                self.add_to_previous_maps(new_map)
        except Exception as exception:
            print('Error: {}'.format(exception))

    # Совершить следующий шаг
    # Правила:
    # * в пустой (мёртвой) клетке, рядом с которой ровно три живые клетки, зарождается жизнь;
    # * если у живой клетки есть две или три живые соседки, то эта клетка продолжает жить;
    # * в противном случае, если соседей меньше двух или больше трёх, клетка умирает
    def next_step(self):
        field_map = self.get_field().get_map()
        # создаем полную копию карты
        new_map = copy.deepcopy(field_map)
        for i in range(0, self.get_field().get_n()):
            for j in range(0, self.get_field().get_m()):
                # Если живая клетка
                if field_map[i][j].is_alive():
                    alive_count = self.get_field().get_alive_neigbor_count(field_map[i][j])
                    if alive_count < 2 or alive_count > 3:
                        new_map[i][j].die()
                # Если пустая клетка
                else:
                    # Если ровно 3 живых соседа - в текущей клетке зародается жизнь
                    if self.get_field().get_alive_neigbor_count(field_map[i][j]) == 3:
                        new_map[i][j].born()
        return new_map

    # Надо ли остановиться, т.е. наступило ли хотя бы одно из условий, для окончания
    # Правила:
    # * на поле не останется ни одной «живой» клетки
    # * конфигурация на очередном шаге в точности (без сдвигов и поворотов) повторит себя же на одном из более ранних
    #   шагов (складывается периодическая конфигурация)
    # * при очередном шаге ни одна из клеток не меняет своего состояния (складывается стабильная конфигурация)
    def is_need_to_stop(self):
        if self.get_field().get_alive_cell_count() == 0:
            print('All cells are dead =(')
            return True
        if self.is_stable_configuration():
            print('Current configuration is stable! iteration: {:0.0f}'.format(len(self.get_previous_maps())))
            return True
        if self.is_periodic_configuration():
            print('Current configuration is periodic! iteration: {:0.0f}'.format(len(self.get_previous_maps())))
            return True
        return False

    # Является ли текущая конфигурация стабильной?
    def is_stable_configuration(self):
        if len(self.get_previous_maps()) == 0:
            return False
        # Берем последнюю карту и смотрим, совпадает ли с текущей, если да - значит ничего не изменилось
        last_map = self.get_previous_maps()[-1]
        if self.get_field().is_equal_map(last_map):
            return True
        return False

    # Является ли текуща конфигурация периодической?
    def is_periodic_configuration(self):
        # Если хоть с одной из предыдущих карт совпадает текущая конфигурация, зачит периодичная
        for map in self.get_previous_maps():
            if self.get_field().is_equal_map(map):
                return True
        return False

    # Getters and Setters. Возможно лучше было бы использовать @property
    def get_field(self):
        return self.field

    def set_field(self, field):
        self.field = field

    def get_previous_maps(self):
        return self.previous_maps

    def add_to_previous_maps(self, new_map):
        self.previous_maps.append(new_map)

# -----------------------------------------------------------------------------
# Выполенение
# -----------------------------------------------------------------------------

# Glider
# init_matrix = [
#     [0, 0, 1, 0, 0, 0, 0],
#     [0, 0, 0, 1, 0, 0, 0],
#     [0, 1, 1, 1, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0],
# ]

# Block
# init_matrix = [
#     [0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 1, 1, 0, 0, 0],
#     [0, 0, 1, 1, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0],
#     [0, 0, 0, 0, 0, 0, 0],
# ]

# Pentadecathlon (period 15)
init_matrix = [
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
]



# run = RunLife(matrix=init_matrix)
run = RunLife(100, 40)
run.run()







